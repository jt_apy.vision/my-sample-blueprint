"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserProtocolPositionDaysResultDto = void 0;
class UserProtocolPositionDaysResultDto {
    constructor(positionIdentifier, date, positionAgeSeconds, blockNumber, timestamp, isEODEntry, positionSharesAtBlock, basePositionShares, positionUsdValueAtBlock, basePositionCostUsd, basePositionUnitCostUsd, netMarketGainUsd, netMarketGainPct, hodlValueUsd, roiVsHodlUsd, roiVsHodlPct, ifHeldAllAmountEth, ifHeldAllAmountEthValueUsd, ifHeldAllAmountBtc, ifHeldAllAmountBtcValueUsd, avgOpenInterestUsd, cumulativeCollectedIncomeUsd, pendingIncomeUsd, incomeApyInception, dayDataTokenLedger, withdrawalOrTransferOut) {
        this.positionIdentifier = positionIdentifier;
        this.date = date;
        this.positionAgeSeconds = positionAgeSeconds;
        this.blockNumber = blockNumber;
        this.timestamp = timestamp;
        this.isEODEntry = isEODEntry;
        this.positionSharesAtBlock = positionSharesAtBlock;
        this.basePositionShares = basePositionShares;
        this.positionUsdValueAtBlock = positionUsdValueAtBlock;
        this.basePositionCostUsd = basePositionCostUsd;
        this.basePositionUnitCostUsd = basePositionUnitCostUsd;
        this.netMarketGainUsd = netMarketGainUsd;
        this.netMarketGainPct = netMarketGainPct;
        this.hodlValueUsd = hodlValueUsd;
        this.roiVsHodlUsd = roiVsHodlUsd;
        this.roiVsHodlPct = roiVsHodlPct;
        this.ifHeldAllAmountEth = ifHeldAllAmountEth;
        this.ifHeldAllAmountEthValueUsd = ifHeldAllAmountEthValueUsd;
        this.ifHeldAllAmountBtc = ifHeldAllAmountBtc;
        this.ifHeldAllAmountBtcValueUsd = ifHeldAllAmountBtcValueUsd;
        this.avgOpenInterestUsd = avgOpenInterestUsd;
        this.cumulativeCollectedIncomeUsd = cumulativeCollectedIncomeUsd;
        this.pendingIncomeUsd = pendingIncomeUsd;
        this.incomeApyInception = incomeApyInception;
        this.dayDataTokenLedger = dayDataTokenLedger;
        this.withdrawalOrTransferOut = withdrawalOrTransferOut;
    }
}
exports.UserProtocolPositionDaysResultDto = UserProtocolPositionDaysResultDto;
