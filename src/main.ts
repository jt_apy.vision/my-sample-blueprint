import { BlockbydateAPI } from './models/blockbydateAPI';
import { BlueprintContext } from './models/blueprintContext';
import MySimpleBlueprint from './simpleBlueprint';
import { DeepMocked, createMock } from '@golevelup/ts-jest';

const mockBlockByDateApi = createMock<BlockbydateAPI>({
  getBlockFromTimestamp(timestamp: number, networkId?: string): Promise<number> {
    return Promise.resolve(1);
  },
});
const context = createMock<BlueprintContext>({
  getBlockByDateApi(): BlockbydateAPI {
    return mockBlockByDateApi;
  },
});

const mySimpleBlueprint = new MySimpleBlueprint(context);

(async function () {
  const userAddress = '0x0000000000000000000000000000000000000001';
  const fromBlock = 16989112;
  try {
    const txns = await mySimpleBlueprint.getUserTransactions(context, [userAddress], fromBlock);
    console.log(txns);

    const operations = await mySimpleBlueprint.classifyTransaction(context, txns.userTransactions[0]);
    console.log(operations);
  } catch (error) {
    console.error(error);
    process.exitCode = 1;
  }
})();
